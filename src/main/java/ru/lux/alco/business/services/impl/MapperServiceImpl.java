package ru.lux.alco.business.services.impl;

import com.github.dozermapper.core.DozerBeanMapperBuilder;
import com.github.dozermapper.core.Mapper;
import org.springframework.stereotype.Service;
import ru.lux.alco.business.services.MapperService;

import java.util.Collection;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class MapperServiceImpl implements MapperService {
    private final Mapper mapper = DozerBeanMapperBuilder.buildDefault();

    @Override
    public <T> T map(Object o, Class<T> clazz) {
        return o == null ? null : mapper.map(o, clazz);
    }

    @Override
    public <T> T map(Object o, Class<T> clazz, String mapId) {
        return o == null ? null : mapper.map(o, clazz, mapId);
    }

    @Override
    public <T> List<T> map(Collection<?> collection, Class<T> clazz) {
        if (collection == null) {
            return null;
        }
        return collection.stream().map(o -> map(o, clazz)).collect(toList());
    }

    @Override
    public <T> List<T> map(Collection<?> collection, Class<T> clazz, String mapId) {
        if (collection == null) {
            return null;
        }
        return collection.stream().map(o -> map(o, clazz, mapId)).collect(toList());
    }
}
