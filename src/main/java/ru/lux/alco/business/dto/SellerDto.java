package ru.lux.alco.business.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SellerDto {

    private Integer id;
    private String name;
    private String surname;
//    @Mapping("position")
    private PositionDto position;
    private Date dateOfBirth;
}
