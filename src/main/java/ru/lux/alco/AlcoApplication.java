package ru.lux.alco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AlcoApplication {

    public static void main(String[] args) {
        SpringApplication.run(AlcoApplication.class, args);
    }

}

