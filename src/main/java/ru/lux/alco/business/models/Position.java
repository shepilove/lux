package ru.lux.alco.business.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
@Table(name = "POSITIONS")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Position {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "positions_id_seq")
    @SequenceGenerator(name = "positions_id_seq", sequenceName = "positions_id_seq", allocationSize = 1)
    private Integer id;
    private String position;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "position")
    @JsonIgnore
    private Set<Seller> sellers;
}
