package ru.lux.alco.business.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.lux.alco.business.dao.ProductRepository;
import ru.lux.alco.business.dto.ProductDto;
import ru.lux.alco.business.services.MapperService;
import ru.lux.alco.business.services.ProductService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final MapperService mapper;

    @Override
    public List<ProductDto> getByShopId(Integer id) {
        return mapper.map(productRepository.findAllByShopId(id), ProductDto.class);
    }
}
