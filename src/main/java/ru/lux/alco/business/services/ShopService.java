package ru.lux.alco.business.services;

import ru.lux.alco.business.dto.ShopDto;
import ru.lux.alco.business.models.Shop;

import java.util.List;

public interface ShopService {

    List<ShopDto> gelAll();

    String add(Shop shop);

    String delete(Integer id);

    String update(Shop shop);
}