package ru.lux.alco.business.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.lux.alco.business.models.Seller;

import java.util.List;

@Repository
public interface SellerRepository extends JpaRepository<Seller, Integer> {

    List<Seller> findAllByShopId(Integer id);
}