package ru.lux.alco.business.services;

import ru.lux.alco.business.dto.ProductDto;

import java.util.List;

public interface ProductService {

    List<ProductDto> getByShopId(Integer id);
}
