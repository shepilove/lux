package ru.lux.alco.business.services;

import ru.lux.alco.business.dto.SellerDto;

import java.util.List;

public interface SellerService {

    List<SellerDto> getByShopId(Integer id);
}
