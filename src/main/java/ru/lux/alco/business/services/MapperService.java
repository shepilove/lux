package ru.lux.alco.business.services;

import java.util.Collection;
import java.util.List;

public interface MapperService {
    <T> T map(Object o, Class<T> clazz);

    <T> T map(Object o, Class<T> clazz, String mapId);

    <T> List<T> map(Collection<?> collection, Class<T> clazz);

    <T> List<T> map(Collection<?> collection, Class<T> clazz, String mapId);
}
