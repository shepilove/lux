package ru.lux.alco.business.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.lux.alco.business.dao.SellerRepository;
import ru.lux.alco.business.dto.SellerDto;
import ru.lux.alco.business.services.MapperService;
import ru.lux.alco.business.services.SellerService;

import java.util.List;

@Service
@RequiredArgsConstructor
public class SellerServiceImpl implements SellerService {

    private final SellerRepository sellerRepository;
    private final MapperService mapper;

    @Override
    public List<SellerDto> getByShopId(Integer id) {
        return mapper.map(sellerRepository.findAllByShopId(id), SellerDto.class);
    }
}
