package ru.lux.alco.business.services.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.lux.alco.business.dao.ShopRepository;
import ru.lux.alco.business.dto.ShopDto;
import ru.lux.alco.business.models.Shop;
import ru.lux.alco.business.services.MapperService;
import ru.lux.alco.business.services.ShopService;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ShopServiceImpl implements ShopService {

    private final ShopRepository shopRepository;
    private final MapperService mapper;

    @Override
    public List<ShopDto> gelAll() {
        return mapper.map(shopRepository.findAll(), ShopDto.class);
    }

    @Override
    public String add(Shop shop) {
        List<Shop> byCodeOrAddress = shopRepository.findByCodeOrAddress(shop.getCode(), shop.getAddress());
        if (byCodeOrAddress.size() > 0) {
            return "Магазин с данным кодом или адресом существует";
        } else {
            shopRepository.save(shop);
            return "Магазин добавлен";
        }
    }

    @Override
    public String delete(Integer id) {
        try {
            shopRepository.deleteById(id);
            return "Магазин удален";
        } catch (Exception e) {
            System.out.println(Arrays.toString(e.getStackTrace()));
            return "Магазина с данным id не существует";
        }
    }

    @Override
    public String update(Shop shop) {
        Optional<Shop> shopFromBase = shopRepository.findById(shop.getId());
        if (shopFromBase.isPresent()) {
            shopRepository.save(shop);
            return "Данные магазина обновлены";
        } else {
            return "Магазина с данным id не существует";
        }
    }
}
