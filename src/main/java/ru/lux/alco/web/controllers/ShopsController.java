package ru.lux.alco.web.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.lux.alco.business.dto.ShopDto;
import ru.lux.alco.business.models.Shop;
import ru.lux.alco.business.services.ShopService;

import java.util.List;

@Slf4j
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api")
public class ShopsController {

    private final ShopService shopService;

    @GetMapping(value = "/shops", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<ShopDto> getAllShops() {
        return shopService.gelAll();
    }

    @PostMapping(value = "/shop/add", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String addNewShop(@RequestBody Shop shop) {
        return shopService.add(shop);
    }

    @DeleteMapping(value = "/shop/delete/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String deleteShop(@PathVariable("id") Integer id){
        return shopService.delete(id);
    }

    @PutMapping(value = "/shop/update", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String updateShop(@RequestBody Shop shop){
        return shopService.update(shop);
    }
}
