package ru.lux.alco.web.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.lux.alco.business.dto.ProductDto;
import ru.lux.alco.business.dto.SellerDto;
import ru.lux.alco.business.services.ProductService;
import ru.lux.alco.business.services.SellerService;

import java.util.List;

@Slf4j
@CrossOrigin
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/api")
public class AlcoController {

    private final SellerService sellerService;
    private final ProductService productService;

    @GetMapping(value = "/hello", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String hello() {
        return "Hello world";
    }

    @GetMapping(value = "/shop/{shopId}/sellers", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<SellerDto> getSellersByShopId(@PathVariable(value = "shopId") Integer id) {
        return sellerService.getByShopId(id);
    }

    @GetMapping(value = "/shop/{shopId}/products", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<ProductDto> getProductsByShopId(@PathVariable(value = "shopId") Integer id) {
        return productService.getByShopId(id);
    }
}
